Changes:
1. New bleeding effect for all ammo types, except 12 gauge saltshot
2. New bleeding damage and AP reduction with scalable severity based on caliber (damage values are configurable via in-game mod options)
3. Bleeding applies only if armor was penetrated
4. Grazing hits bleeding is halved
5. Penetration debuff reduced for HP and Shock ammo types
6. Increased damage for HP ammo type

Feel free to enable/disable this mod mid-game if you are not in the battle

Mod is now compatible with custom ammo and custom weapons mods!
Mod doesn't affect wounds mechanics.
Mod doesn't affect ordnance and melee weapons. So if some unit got both bullet and melee damage, then it will have 2 different bleeding types. Both effects can be healed with first and big aid kits. I'll try to change bleeding effect caused by melee and ordnance attacks, but considering my current knowledge of this game it seems like an impossible task.

This mod also avaliable at:
- Steam Workshop: https://steamcommunity.com/sharedfiles/filedetails/?id=3024279385
- NexusMods: https://www.nexusmods.com/jaggedalliance3/mods/141

How to install:
1. Extract "Bleeding Mod" folder to %AppData%/Roaming/Jagged Alliance 3/mods
2. Enable mod via in-game mod manager
3. Enjoy!
