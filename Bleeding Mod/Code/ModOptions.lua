function OnMsg.ApplyModOptions()
    BleedingModConfigValues.BleedingAmount_12gauge = tonumber(string.sub(CurrentModOptions.id_BleedingAmount_12gauge, 1, 2))
    BleedingModConfigValues.BleedingAmount_556 = tonumber(string.sub(CurrentModOptions.id_BleedingAmount_556, 1, 2))
    BleedingModConfigValues.BleedingAmount_762 = tonumber(string.sub(CurrentModOptions.id_BleedingAmount_762, 1, 2))
    BleedingModConfigValues.BleedingAmount_9mm = tonumber(string.sub(CurrentModOptions.id_BleedingAmount_9mm, 1, 2))
    BleedingModConfigValues.BleedingAmount_44CAL = tonumber(string.sub(CurrentModOptions.id_BleedingAmount_44CAL, 1, 2))
    BleedingModConfigValues.BleedingAmount_50BMG = tonumber(string.sub(CurrentModOptions.id_BleedingAmount_50BMG, 1, 2))
    BleedingModConfigValues.BleedingAmount_Other = tonumber(string.sub(CurrentModOptions.id_BleedingAmount_Other, 1, 2))
    BleedingModConfigValues.EnableFriendlyFireBleeding = CurrentModOptions.id_EnableFriendlyFireBleeding
end
function OnMsg.NewMap()
    BleedingModConfigValues.BleedingAmount_12gauge = tonumber(string.sub(CurrentModOptions.id_BleedingAmount_12gauge, 1, 2))
    BleedingModConfigValues.BleedingAmount_556 = tonumber(string.sub(CurrentModOptions.id_BleedingAmount_556, 1, 2))
    BleedingModConfigValues.BleedingAmount_762 = tonumber(string.sub(CurrentModOptions.id_BleedingAmount_762, 1, 2))
    BleedingModConfigValues.BleedingAmount_9mm = tonumber(string.sub(CurrentModOptions.id_BleedingAmount_9mm, 1, 2))
    BleedingModConfigValues.BleedingAmount_44CAL = tonumber(string.sub(CurrentModOptions.id_BleedingAmount_44CAL, 1, 2))
    BleedingModConfigValues.BleedingAmount_50BMG = tonumber(string.sub(CurrentModOptions.id_BleedingAmount_50BMG, 1, 2))
    BleedingModConfigValues.BleedingAmount_Other = tonumber(string.sub(CurrentModOptions.id_BleedingAmount_Other, 1, 2))
    BleedingModConfigValues.EnableFriendlyFireBleeding = CurrentModOptions.id_EnableFriendlyFireBleeding
end