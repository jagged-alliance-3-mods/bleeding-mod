local function GetBleedingAmount(tab, val)
    for index, value in pairs(tab) do
        if index == val then
            return value
        end
    end
    return false
end
function OnMsg.DamageTaken(attacker, target, dmg, hit_descr)
    local damage_result = 0
    local weapon_used = {}
    local caliber_switch = {
        ["12gauge"]  = BleedingModConfigValues.BleedingAmount_12gauge,
        ["556"] = BleedingModConfigValues.BleedingAmount_556,
        ["762NATO"] = BleedingModConfigValues.BleedingAmount_762,
        ["762WP"] = BleedingModConfigValues.BleedingAmount_762,
        ["9mm"] = BleedingModConfigValues.BleedingAmount_9mm,
        ["44CAL"] = BleedingModConfigValues.BleedingAmount_44CAL,
        ["50BMG"] = BleedingModConfigValues.BleedingAmount_50BMG,
    }
    if hit_descr.ally_hit == true and BleedingModConfigValues.EnableFriendlyFireBleeding == false then
        return
    else
        if hit_descr.weapon == nil then
            return
        else
            weapon_used = hit_descr.weapon
            if weapon_used.base_Caliber == nil then
                return
            else
                if GetBleedingAmount(caliber_switch, weapon_used.base_Caliber) == false then
                    damage_result = BleedingModConfigValues.BleedingAmount_Other
                else
                    damage_result = GetBleedingAmount(caliber_switch, weapon_used.base_Caliber)
                end
            end
        end
    end
    if hit_descr.grazing_reason ~= false then
        damage_result = damage_result / 2
    end
    if (next(hit_descr.armor_pen) ~= nil) or hit_descr.armor_prevented == 0 then
        if damage_result > 0 then
            for i = damage_result, 1, -1 do
                target:AddStatusEffect("BleedingStacks")
            end
            target:AddStatusEffect("BleedingStacksMessage")
            target:RemoveStatusEffect("BleedingStacksMessage")
        end
    end
end