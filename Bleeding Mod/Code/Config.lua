BleedingModConfigValues = {
    BleedingAmount_12gauge = 2,
    BleedingAmount_556 = 3,
    BleedingAmount_762 = 4,
    BleedingAmount_9mm = 5,
    BleedingAmount_44CAL = 6,
    BleedingAmount_50BMG = 8,
    BleedingAmount_Other = 5,
    EnableFriendlyFireBleeding = true,
}