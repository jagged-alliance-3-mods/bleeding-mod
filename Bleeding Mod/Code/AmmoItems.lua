--Ammo modifications
_556_HP.Modifications = {
    PlaceObj('CaliberModification', {
        mod_add = 50,
        target_prop = "CritChance",
    }),
    PlaceObj('CaliberModification', {
        mod_add = -1,
        target_prop = "PenetrationClass",
    }),
    PlaceObj('CaliberModification', {
        mod_add = 2,
        target_prop = "Damage",
    }),
}
_762NATO_HP.Modifications = {
    PlaceObj('CaliberModification', {
        mod_add = 50,
        target_prop = "CritChance",
    }),
    PlaceObj('CaliberModification', {
        mod_add = -1,
        target_prop = "PenetrationClass",
    }),
    PlaceObj('CaliberModification', {
        mod_add = 3,
        target_prop = "Damage",
    }),
}
_762WP_HP.Modifications = {
    PlaceObj('CaliberModification', {
        mod_add = 50,
        target_prop = "CritChance",
    }),
    PlaceObj('CaliberModification', {
        mod_add = -1,
        target_prop = "PenetrationClass",
    }),
    PlaceObj('CaliberModification', {
        mod_add = 3,
        target_prop = "Damage",
    }),
}
_9mm_HP.Modifications = {
    PlaceObj('CaliberModification', {
        mod_add = 50,
        target_prop = "CritChance",
    }),
    PlaceObj('CaliberModification', {
        mod_add = -1,
        target_prop = "PenetrationClass",
    }),
    PlaceObj('CaliberModification', {
        mod_add = 2,
        target_prop = "Damage",
    }),
}
_9mm_Shock.Modifications = {
    PlaceObj('CaliberModification', {
        mod_add = 50,
        target_prop = "CritChance",
    }),
    PlaceObj('CaliberModification', {
        mod_add = -1,
        target_prop = "PenetrationClass",
    }),
    PlaceObj('CaliberModification', {
        mod_add = -4,
        target_prop = "WeaponRange",
    }),
    PlaceObj('CaliberModification', {
        mod_add = 2,
        target_prop = "Damage",
    }),
}
_44CAL_HP.Modifications = {
    PlaceObj('CaliberModification', {
        mod_add = 50,
        target_prop = "CritChance",
    }),
    PlaceObj('CaliberModification', {
        mod_add = -1,
        target_prop = "PenetrationClass",
    }),
    PlaceObj('CaliberModification', {
        mod_add = 3,
        target_prop = "Damage",
    }),
}
_44CAL_Shock.Modifications = {
    PlaceObj('CaliberModification', {
        mod_add = 50,
        target_prop = "CritChance",
    }),
    PlaceObj('CaliberModification', {
        mod_add = -4,
        target_prop = "WeaponRange",
    }),
    PlaceObj('CaliberModification', {
        mod_add = -1,
        target_prop = "PenetrationClass",
    }),
    PlaceObj('CaliberModification', {
        mod_add = 3,
        target_prop = "Damage",
    }),
}
_50BMG_HE.Modifications = {
    PlaceObj('CaliberModification', {
        mod_add = 50,
        target_prop = "CritChance",
    }),
    PlaceObj('CaliberModification', {
        mod_add = -1,
        target_prop = "PenetrationClass",
    }),
    PlaceObj('CaliberModification', {
        mod_add = 5,
        target_prop = "Damage",
    }),
}
