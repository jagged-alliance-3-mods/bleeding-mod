return {
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_556_AP",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(447044880769, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Improved armor penetration\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_556_Basic",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(304363100733, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_556_HP",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(534464439678, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Reduced armor penetration\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> High Crit chance\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Increased damage\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_556_Match",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(215531110551, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Increased bonus from Aiming\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_556_Tracer",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(113488418621, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Hit enemies are <color EmStyle>Exposed</color> and lose the benefits of Cover\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_762NATO_AP",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(900624000034, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Improved armor penetration\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_762NATO_Basic",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(211931816526, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Improved armor penetration"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_762NATO_HP",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(530149966145, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Reduced armor penetration\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> High Crit chance\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Increased damage\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_762NATO_Match",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(383926108715, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Increased bonus from Aiming\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_762NATO_Tracer",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(716314479445, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Hit enemies are <color EmStyle>Exposed</color> and lose the benefits of Cover\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_762WP_AP",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(155327521719, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Improved armor penetration\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_762WP_Basic",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(466943469543, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_762WP_HP",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(586939839607, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Reduced armor penetration\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> High Crit chance\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Increased damage\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_762WP_Match",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(981047901270, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Increased bonus from Aiming\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_762WP_Tracer",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(961477449022, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Hit enemies are <color EmStyle>Exposed</color> and lose the benefits of Cover\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_9mm_AP",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(378800701034, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Improved armor penetration\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_9mm_Basic",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(758984686947, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_9mm_HP",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(166959730114, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Reduced armor penetration\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> High Crit chance\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Increased damage\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_9mm_Match",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(830267221073, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Increased bonus from Aiming\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_9mm_Shock",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(267180118796, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Reduced range\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Reduced armor penetration\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> High Crit chance\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Increased damage\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Hit enemies are <color EmStyle>Exposed</color> and lose the benefits of Cover\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_9mm_Subsonic",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(905080488507, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Less noisy\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_9mm_Tracer",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(705107444812, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Hit enemies are <color EmStyle>Exposed</color> and lose the benefits of Cover\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_44CAL_AP",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(305982434128, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Improved armor penetration\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_44CAL_Basic",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(467025234074, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_44CAL_HP",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(265028771111, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Reduced armor penetration\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> High Crit chance\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Increased damage\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_44CAL_Match",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(982140641970, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Increased bonus from Aiming\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_44CAL_Shock",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(449282233803, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Reduced range\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Reduced armor penetration\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> High Crit chance\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Increased damage\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Hit enemies are <color EmStyle>Exposed</color> and lose the benefits of Cover\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_50BMG_Basic",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(191743949903, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_50BMG_HE",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(756958101250, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Reduced armor penetration\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> High Crit chance\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Increased damage\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_50BMG_Incendiary",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(941002789505, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Hit enemies are <color EmStyle>Exposed</color> and lose the benefits of Cover\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Burning</color>\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "InventoryItemCompositeDef",
	'TargetId', "_50BMG_SLAP",
	'TargetProp', "AdditionalHint",
	'TargetValue', T(101275324528, "<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Improved armor penetration\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Slightly higher Crit chance\n<image UI/Conversation/T_Dialogue_IconBackgroundCircle.tga 400 130 128 120> Inflicts <color EmStyle>Bleeding</color>"),
}),
PlaceObj('ModItemCharacterEffectCompositeDef', {
	'Id', "BleedingStacks",
	'Parameters', {
		PlaceObj('PresetParamNumber', {
			'Name', "DamagePerTurn",
			'Value', 1,
			'Tag', "<DamagePerTurn>",
		}),
		PlaceObj('PresetParamNumber', {
			'Name', "APLoss",
			'Value', -1,
			'Tag', "<APLoss>",
		}),
		PlaceObj('PresetParamPercent', {
			'Name', "cth_penalty",
			'Value', -1,
			'Tag', "<cth_penalty>%",
		}),
		PlaceObj('PresetParamNumber', {
			'Name', "APLossFactor",
			'Value', 4,
			'Tag', "<APLossFactor>",
		}),
	},
	'object_class', "StatusEffect",
	'msg_reactions', {
		PlaceObj('MsgReaction', {
			Event = "StatusEffectRemoved",
			Handler = function (self, obj, id, stacks, reason)
				local reaction_idx = table.find(self.msg_reactions or empty_table, "Event", "StatusEffectRemoved")
				if not reaction_idx then return end
				
				local function exec(self, obj, id, stacks, reason)
				local effect = obj:GetStatusEffect("BleedingStacks")
				if g_Teams[g_CurrentTeam] == obj.team and not obj:HasStatusEffect("BeingBandaged") then
					obj:GainAP(-self:ResolveValue("APLoss") * (effect.stacks + 1) / self:ResolveValue("APLossFactor") * const.Scale.AP)
				end
				end
				local _id = GetCharacterEffectId(self)
				if _id == id then exec(self, obj, id, stacks, reason) end
				
			end,
			HandlerCode = function (self, obj, id, stacks, reason)
				local effect = obj:GetStatusEffect("BleedingStacks")
				if g_Teams[g_CurrentTeam] == obj.team and not obj:HasStatusEffect("BeingBandaged") then
					obj:GainAP(-self:ResolveValue("APLoss") * (effect.stacks + 1) / self:ResolveValue("APLossFactor") * const.Scale.AP)
				end
			end,
			param_bindings = false,
		}),
		PlaceObj('MsgReaction', {
			Event = "UnitEndTurn",
			Handler = function (self, unit)
				local reaction_idx = table.find(self.msg_reactions or empty_table, "Event", "UnitEndTurn")
				if not reaction_idx then return end
				
				local function exec(self, unit)
				local effect = unit:GetStatusEffect("BleedingStacks")
				if not IsInCombat() then return end
				if unit:HasStatusEffect("BeingBandaged") then
					for i = effect.stacks, 1, -1 do
						unit:RemoveStatusEffect("BleedingStacks")
					end
					return
				end
				local dmgresult = self:ResolveValue("DamagePerTurn") * effect.stacks
				local floating_text = T{193053798048, "<num> (bleeding)", num = dmgresult}
				local pov_team = GetPoVTeam()
				local has_visibility = HasVisibilityTo(pov_team, unit)
				local log_msg = T{729241506274, "<name> bleeds for <em><num> damage</em>", name = unit:GetLogName(), num = dmgresult}
				unit:TakeDirectDamage(dmgresult, has_visibility and floating_text or false, "short", log_msg)
				end
				local id = GetCharacterEffectId(self)
				
				if id then
					if IsKindOf(unit, "StatusEffectObject") and unit:HasStatusEffect(id) then
						exec(self, unit)
					end
				else
					exec(self, unit)
				end
				
			end,
			HandlerCode = function (self, unit)
				local effect = unit:GetStatusEffect("BleedingStacks")
				if not IsInCombat() then return end
				if unit:HasStatusEffect("BeingBandaged") then
					for i = effect.stacks, 1, -1 do
						unit:RemoveStatusEffect("BleedingStacks")
					end
					return
				end
				local dmgresult = self:ResolveValue("DamagePerTurn") * effect.stacks
				local floating_text = T{193053798048, "<num> (bleeding)", num = dmgresult}
				local pov_team = GetPoVTeam()
				local has_visibility = HasVisibilityTo(pov_team, unit)
				local log_msg = T{729241506274, "<name> bleeds for <em><num> damage</em>", name = unit:GetLogName(), num = dmgresult}
				unit:TakeDirectDamage(dmgresult, has_visibility and floating_text or false, "short", log_msg)
			end,
			param_bindings = false,
		}),
		PlaceObj('MsgReaction', {
			Event = "UnitBeginTurn",
			Handler = function (self, unit)
				local reaction_idx = table.find(self.msg_reactions or empty_table, "Event", "UnitBeginTurn")
				if not reaction_idx then return end
				
				local function exec(self, unit)
				local effect = unit:GetStatusEffect("BleedingStacks")
				unit:ConsumeAP(1 * effect.stacks / 4 * const.Scale.AP)
				end
				local id = GetCharacterEffectId(self)
				
				if id then
					if IsKindOf(unit, "StatusEffectObject") and unit:HasStatusEffect(id) then
						exec(self, unit)
					end
				else
					exec(self, unit)
				end
				
			end,
			HandlerCode = function (self, unit)
				local effect = unit:GetStatusEffect("BleedingStacks")
				unit:ConsumeAP(1 * effect.stacks / 4 * const.Scale.AP)
			end,
			param_bindings = false,
		}),
		PlaceObj('MsgReaction', {
			Event = "GatherCTHModifications",
			Handler = function (self, attacker, cth_id, data)
				local reaction_idx = table.find(self.msg_reactions or empty_table, "Event", "GatherCTHModifications")
				if not reaction_idx then return end
				
				local function exec(self, attacker, cth_id, data)
				if cth_id == self.id then
					data.mod_add = data.mod_add + self:ResolveValue("cth_penalty")
				end
				end
				local id = GetCharacterEffectId(self)
				
				if id then
					if IsKindOf(attacker, "StatusEffectObject") and attacker:HasStatusEffect(id) then
						exec(self, attacker, cth_id, data)
					end
				else
					exec(self, attacker, cth_id, data)
				end
				
			end,
			HandlerCode = function (self, attacker, cth_id, data)
				if cth_id == self.id then
					data.mod_add = data.mod_add + self:ResolveValue("cth_penalty")
				end
			end,
			param_bindings = false,
		}),
	},
	'DisplayName', T(486437174920, --[[ModItemCharacterEffectCompositeDef BleedingStacks DisplayName]] "Bleeding"),
	'Description', T(446816475256, --[[ModItemCharacterEffectCompositeDef BleedingStacks Description]] "This character will take <color EmStyle>Damage</color> each turn until they are <color EmStyle>bandaged</color>. <color EmStyle>AP decreased by <APLoss></color> for every <color EmStyle><APLossFactor> bleeding damage</color>."),
	'type', "Debuff",
	'Icon', "Mod/pRbkRtK/images/icons/bleeding_penetrated.png",
	'max_stacks', 99,
	'RemoveOnEndCombat', true,
	'RemoveOnSatViewTravel', true,
	'Shown', true,
}),
PlaceObj('ModItemCharacterEffectCompositeDef', {
	'Group', "System",
	'Id', "BandageInCombat",
	'comment', "Replace with modified GetBandaged function",
	'object_class', "StatusEffect",
	'msg_reactions', {
		PlaceObj('MsgReaction', {
			Event = "StatusEffectAdded",
			Handler = function (self, obj, id, stacks)
				local reaction_idx = table.find(self.msg_reactions or empty_table, "Event", "StatusEffectAdded")
				if not reaction_idx then return end
				
				local function exec(self, obj, id, stacks)
				local target = IsKindOf(obj, "Unit") and obj:GetBandageTarget()
				if target then
					target:RemoveStatusEffect("Downed")
					target:RemoveStatusEffect("BleedingOut")
				end
				obj:RemoveStatusEffect("FreeMove")
				end
				local _id = GetCharacterEffectId(self)
				if _id == id then exec(self, obj, id, stacks) end
				
			end,
			HandlerCode = function (self, obj, id, stacks)
				local target = IsKindOf(obj, "Unit") and obj:GetBandageTarget()
				if target then
					target:RemoveStatusEffect("Downed")
					target:RemoveStatusEffect("BleedingOut")
				end
				obj:RemoveStatusEffect("FreeMove")
			end,
			param_bindings = false,
		}),
		PlaceObj('MsgReaction', {
			Event = "StatusEffectRemoved",
			Handler = function (self, obj, id, stacks, reason)
				local reaction_idx = table.find(self.msg_reactions or empty_table, "Event", "StatusEffectRemoved")
				if not reaction_idx then return end
				
				local function exec(self, obj, id, stacks, reason)
				local target =  IsKindOf(obj, "Unit") and obj:GetBandageTarget()
				if not g_Combat then return end
				if target and target:IsDowned() and not target:HasStatusEffect("Unconscious") then
					target:RemoveStatusEffect("Stabilized")
					target:AddStatusEffect("BleedingOut")
					target:RemoveStatusEffect("BeingBandaged")
				end
				
				if CurrentThread() == obj.command_thread then
					obj:QueueCommand("EndCombatBandage") -- make sure it does not break the RemoveStatusEffect call
				else
					obj:SetCommand("EndCombatBandage")
				end
				end
				local _id = GetCharacterEffectId(self)
				if _id == id then exec(self, obj, id, stacks, reason) end
				
			end,
			HandlerCode = function (self, obj, id, stacks, reason)
				local target =  IsKindOf(obj, "Unit") and obj:GetBandageTarget()
				if not g_Combat then return end
				if target and target:IsDowned() and not target:HasStatusEffect("Unconscious") then
					target:RemoveStatusEffect("Stabilized")
					target:AddStatusEffect("BleedingOut")
					target:RemoveStatusEffect("BeingBandaged")
				end
				
				if CurrentThread() == obj.command_thread then
					obj:QueueCommand("EndCombatBandage") -- make sure it does not break the RemoveStatusEffect call
				else
					obj:SetCommand("EndCombatBandage")
				end
			end,
			param_bindings = false,
		}),
		PlaceObj('MsgReaction', {
			Event = "UnitBeginTurn",
			Handler = function (self, unit)
				local reaction_idx = table.find(self.msg_reactions or empty_table, "Event", "UnitBeginTurn")
				if not reaction_idx then return end
				
				local function exec(self, unit)
				local target = unit:GetBandageTarget()
				local medicine = unit:GetBandageMedicine()
				if not target or not medicine or target.command == "Die" or target:IsDead() or target.HitPoints >= target.MaxHitPoints then
					unit:RemoveStatusEffect("BandageInCombat")
					return 
				end
				end
				local id = GetCharacterEffectId(self)
				
				if id then
					if IsKindOf(unit, "StatusEffectObject") and unit:HasStatusEffect(id) then
						exec(self, unit)
					end
				else
					exec(self, unit)
				end
				
			end,
			HandlerCode = function (self, unit)
				local target = unit:GetBandageTarget()
				local medicine = unit:GetBandageMedicine()
				if not target or not medicine or target.command == "Die" or target:IsDead() or target.HitPoints >= target.MaxHitPoints then
					unit:RemoveStatusEffect("BandageInCombat")
					return 
				end
			end,
			param_bindings = false,
		}),
		PlaceObj('MsgReaction', {
			Event = "UnitEndTurn",
			Handler = function (self, unit)
				local reaction_idx = table.find(self.msg_reactions or empty_table, "Event", "UnitEndTurn")
				if not reaction_idx then return end
				
				local function exec(self, unit)
				local target = unit:GetBandageTarget()
				local targeteffect = target:GetStatusEffect("BleedingStacks")  
				--better to override GetBandaged function, but I don't know where to find it yet
				local medicine = unit:GetBandageMedicine()
				if not IsValid(target) or target.command == "Die" or target:IsDead() or target.HitPoints >= target.MaxHitPoints then
					unit:RemoveStatusEffect(self.id)
					return
				end
				if target:IsDowned() then
					if target:GetEffectValue("stabilized") or RollSkillCheck(unit, "Medical") then
						target:SetCommand("DownedRally", unit, medicine)
					else
						target:AddStatusEffect("Stabilized")
					end
				else
					target:GetBandaged(medicine, unit)
					if target.HitPoints >= target.MaxHitPoints then
						target:RemoveStatusEffect("BeingBandaged")
						unit:RemoveStatusEffect(self.id)
					end
					for i = targeteffect.stacks, 1, -1 do
						target:RemoveStatusEffect("BleedingStacks")
					end
				end
				end
				local id = GetCharacterEffectId(self)
				
				if id then
					if IsKindOf(unit, "StatusEffectObject") and unit:HasStatusEffect(id) then
						exec(self, unit)
					end
				else
					exec(self, unit)
				end
				
			end,
			HandlerCode = function (self, unit)
				local target = unit:GetBandageTarget()
				local targeteffect = target:GetStatusEffect("BleedingStacks")  
				--better to override GetBandaged function, but I don't know where to find it yet
				local medicine = unit:GetBandageMedicine()
				if not IsValid(target) or target.command == "Die" or target:IsDead() or target.HitPoints >= target.MaxHitPoints then
					unit:RemoveStatusEffect(self.id)
					return
				end
				if target:IsDowned() then
					if target:GetEffectValue("stabilized") or RollSkillCheck(unit, "Medical") then
						target:SetCommand("DownedRally", unit, medicine)
					else
						target:AddStatusEffect("Stabilized")
					end
				else
					target:GetBandaged(medicine, unit)
					if target.HitPoints >= target.MaxHitPoints then
						target:RemoveStatusEffect("BeingBandaged")
						unit:RemoveStatusEffect(self.id)
					end
					for i = targeteffect.stacks, 1, -1 do
						target:RemoveStatusEffect("BleedingStacks")
					end
				end
			end,
			param_bindings = false,
		}),
	},
	'DisplayName', T(743469201622, --[[ModItemCharacterEffectCompositeDef BandageInCombat DisplayName]] "Treating"),
	'Description', T(307131916085, --[[ModItemCharacterEffectCompositeDef BandageInCombat Description]] "Bandaging an ally. No more actions available this turn. Effectiveness of the action depends on Medical skill."),
	'Icon', "UI/Hud/Status effects/treating",
	'RemoveOnSatViewTravel', true,
	'Shown', true,
}),
PlaceObj('ModItemCharacterEffectCompositeDef', {
	'Group', "Perk-NPC",
	'Id', "ZombiePerk",
	'comment', "Replace with dynamic immunity check on object",
	'object_class', "Perk",
	'msg_reactions', {
		PlaceObj('MsgReaction', {
			Event = "StatusEffectAdded",
			Handler = function (self, obj, id, stacks)
				local reaction_idx = table.find(self.msg_reactions or empty_table, "Event", "StatusEffectAdded")
				if not reaction_idx then return end
				
				local function exec(self, obj, id, stacks)
				obj:AddStatusEffectImmunity("Suppressed", id)
				obj:AddStatusEffectImmunity("Bleeding", id)
				obj:AddStatusEffectImmunity("BleedingStacks", id)
				obj:AddStatusEffectImmunity("Inaccurate", id)
				obj:AddStatusEffectImmunity("Flanked", id)
				obj:AddStatusEffectImmunity("SuppressionChangeStance", id)
				end
				local _id = GetCharacterEffectId(self)
				if _id == id then exec(self, obj, id, stacks) end
				
			end,
			HandlerCode = function (self, obj, id, stacks)
				obj:AddStatusEffectImmunity("Suppressed", id)
				obj:AddStatusEffectImmunity("Bleeding", id)
				obj:AddStatusEffectImmunity("BleedingStacks", id)
				obj:AddStatusEffectImmunity("Inaccurate", id)
				obj:AddStatusEffectImmunity("Flanked", id)
				obj:AddStatusEffectImmunity("SuppressionChangeStance", id)
			end,
			param_bindings = false,
		}),
		PlaceObj('MsgReaction', {
			Event = "StatusEffectRemoved",
			Handler = function (self, obj, id, stacks, reason)
				local reaction_idx = table.find(self.msg_reactions or empty_table, "Event", "StatusEffectRemoved")
				if not reaction_idx then return end
				
				local function exec(self, obj, id, stacks, reason)
				obj:RemoveStatusEffectImmunity("Suppressed", id)
				obj:RemoveStatusEffectImmunity("Bleeding", id)
				obj:RemoveStatusEffectImmunity("BleedingStacks", id)
				obj:RemoveStatusEffectImmunity("Inaccurate", id)
				obj:RemoveStatusEffectImmunity("Flanked", id)
				obj:RemoveStatusEffectImmunity("SuppressionChangeStance", id)
				end
				local _id = GetCharacterEffectId(self)
				if _id == id then exec(self, obj, id, stacks, reason) end
				
			end,
			HandlerCode = function (self, obj, id, stacks, reason)
				obj:RemoveStatusEffectImmunity("Suppressed", id)
				obj:RemoveStatusEffectImmunity("Bleeding", id)
				obj:RemoveStatusEffectImmunity("BleedingStacks", id)
				obj:RemoveStatusEffectImmunity("Inaccurate", id)
				obj:RemoveStatusEffectImmunity("Flanked", id)
				obj:RemoveStatusEffectImmunity("SuppressionChangeStance", id)
			end,
			param_bindings = false,
		}),
	},
	'DisplayName', T(158285288253, --[[ModItemCharacterEffectCompositeDef ZombiePerk DisplayName]] "Infected"),
	'Description', T(706602029832, --[[ModItemCharacterEffectCompositeDef ZombiePerk Description]] "Immune to Suppressed, Bleeding, Inaccurate, and Flanked."),
	'Icon', "UI/Hud/Status effects/stabilized",
	'Shown', true,
}),
PlaceObj('ModItemCharacterEffectCompositeDef', {
	'Group', "Perk-NPC",
	'Id', "DieselPerk",
	'comment', "Replace with dynamic immunity check on object",
	'object_class', "Perk",
	'msg_reactions', {
		PlaceObj('MsgReaction', {
			Event = "StatusEffectAdded",
			Handler = function (self, obj, id, stacks)
				local reaction_idx = table.find(self.msg_reactions or empty_table, "Event", "StatusEffectAdded")
				if not reaction_idx then return end
				
				local function exec(self, obj, id, stacks)
				obj:AddStatusEffectImmunity("Suppressed", id)
				obj:AddStatusEffectImmunity("Bleeding", id)
				obj:AddStatusEffectImmunity("BleedingStacks", id)
				obj:AddStatusEffectImmunity("Inaccurate", id)
				obj:AddStatusEffectImmunity("Flanked", id)
				end
				local _id = GetCharacterEffectId(self)
				if _id == id then exec(self, obj, id, stacks) end
				
			end,
			HandlerCode = function (self, obj, id, stacks)
				obj:AddStatusEffectImmunity("Suppressed", id)
				obj:AddStatusEffectImmunity("Bleeding", id)
				obj:AddStatusEffectImmunity("BleedingStacks", id)
				obj:AddStatusEffectImmunity("Inaccurate", id)
				obj:AddStatusEffectImmunity("Flanked", id)
			end,
			param_bindings = false,
		}),
		PlaceObj('MsgReaction', {
			Event = "StatusEffectRemoved",
			Handler = function (self, obj, id, stacks, reason)
				local reaction_idx = table.find(self.msg_reactions or empty_table, "Event", "StatusEffectRemoved")
				if not reaction_idx then return end
				
				local function exec(self, obj, id, stacks, reason)
				obj:RemoveStatusEffectImmunity("Suppressed", id)
				obj:RemoveStatusEffectImmunity("Bleeding", id)
				obj:RemoveStatusEffectImmunity("BleedingStacks", id)
				obj:RemoveStatusEffectImmunity("Inaccurate", id)
				obj:RemoveStatusEffectImmunity("Flanked", id)
				end
				local _id = GetCharacterEffectId(self)
				if _id == id then exec(self, obj, id, stacks, reason) end
				
			end,
			HandlerCode = function (self, obj, id, stacks, reason)
				obj:RemoveStatusEffectImmunity("Suppressed", id)
				obj:RemoveStatusEffectImmunity("Bleeding", id)
				obj:RemoveStatusEffectImmunity("BleedingStacks", id)
				obj:RemoveStatusEffectImmunity("Inaccurate", id)
				obj:RemoveStatusEffectImmunity("Flanked", id)
			end,
			param_bindings = false,
		}),
	},
	'DisplayName', T(564742346450, --[[ModItemCharacterEffectCompositeDef DieselPerk DisplayName]] "Enhanced"),
	'Description', T(925614208510, --[[ModItemCharacterEffectCompositeDef DieselPerk Description]] "Immune to Suppressed, Bleeding, Inaccurate, and Flanked."),
	'Icon', "UI/Hud/Status effects/stimmed",
	'Shown', true,
}),
PlaceObj('ModItemCharacterEffectCompositeDef', {
	'Group', "System",
	'Id', "BleedingStacksMessage",
	'comment', "Replase with custom floating text function",
	'object_class', "CharacterEffect",
	'DisplayName', T(750385501935, --[[ModItemCharacterEffectCompositeDef BleedingStacksMessage DisplayName]] "Bleeding"),
	'RemoveEffectText', T(923209475182, --[[ModItemCharacterEffectCompositeDef BleedingStacksMessage RemoveEffectText]] "<color EmStyle><DisplayName></color>"),
	'lifetime', "Until End of Turn",
	'RemoveOnEndCombat', true,
	'RemoveOnSatViewTravel', true,
	'HasFloatingText', true,
}),
PlaceObj('ModItemCode', {
	'name', "AmmoItems",
	'CodeFileName', "Code/AmmoItems.lua",
}),
PlaceObj('ModItemCode', {
	'name', "BleedingStacksOnHit",
	'CodeFileName', "Code/BleedingStacksOnHit.lua",
}),
PlaceObj('ModItemCode', {
	'name', "Config",
	'CodeFileName', "Code/Config.lua",
}),
PlaceObj('ModItemCode', {
	'name', "ModOptions",
	'CodeFileName', "Code/ModOptions.lua",
}),
PlaceObj('ModItemOptionChoice', {
	'name', "id_BleedingAmount_12gauge",
	'DisplayName', "Bleeding damage 12gauge",
	'Help', "Bleeding damage inflicted by 12gauge caliber",
	'DefaultValue', "2 (Default)",
	'ChoiceList', {
		"0 (Disable bleeding)",
		"1",
		"2 (Default)",
		"3",
		"4",
		"5",
		"6",
		"7",
		"8",
		"9",
		"10",
		"11",
		"12",
		"13",
		"14",
		"15",
		"16",
		"17",
		"18",
		"19",
		"20 (Are you serious?)",
	},
}),
PlaceObj('ModItemOptionChoice', {
	'name', "id_BleedingAmount_44CAL",
	'DisplayName', "Bleeding damage 44CAL",
	'Help', "Bleeding damage inflicted by .44 caliber",
	'DefaultValue', "6 (Default)",
	'ChoiceList', {
		"0 (Disable bleeding)",
		"1",
		"2",
		"3",
		"4",
		"5",
		"6 (Default)",
		"7",
		"8",
		"9",
		"10",
		"11",
		"12",
		"13",
		"14",
		"15",
		"16",
		"17",
		"18",
		"19",
		"20 (Are you serious?)",
	},
}),
PlaceObj('ModItemOptionChoice', {
	'name', "id_BleedingAmount_50BMG",
	'DisplayName', "Bleeding damage 50BMG",
	'Help', "Bleeding damage inflicted by 50BMG caliber",
	'DefaultValue', "8 (Default)",
	'ChoiceList', {
		"0 (Disable bleeding)",
		"1",
		"2",
		"3",
		"4",
		"5",
		"6",
		"7",
		"8 (Default)",
		"9",
		"10",
		"11",
		"12",
		"13",
		"14",
		"15",
		"16",
		"17",
		"18",
		"19",
		"20 (Are you serious?)",
	},
}),
PlaceObj('ModItemOptionChoice', {
	'name', "id_BleedingAmount_556",
	'DisplayName', "Bleeding damage 556",
	'Help', "Bleeding damage inflicted by 556 caliber",
	'DefaultValue', "3 (Default)",
	'ChoiceList', {
		"0 (Disable bleeding)",
		"1",
		"2",
		"3 (Default)",
		"4",
		"5",
		"6",
		"7",
		"8",
		"9",
		"10",
		"11",
		"12",
		"13",
		"14",
		"15",
		"16",
		"17",
		"18",
		"19",
		"20 (Are you serious?)",
	},
}),
PlaceObj('ModItemOptionChoice', {
	'name', "id_BleedingAmount_762",
	'DisplayName', "Bleeding damage 762",
	'Help', "Bleeding damage inflicted by 762WP and 762NATO calibers",
	'DefaultValue', "4 (Default)",
	'ChoiceList', {
		"0 (Disable bleeding)",
		"1",
		"2",
		"3",
		"4 (Default)",
		"5",
		"6",
		"7",
		"8",
		"9",
		"10",
		"11",
		"12",
		"13",
		"14",
		"15",
		"16",
		"17",
		"18",
		"19",
		"20 (Are you serious?)",
	},
}),
PlaceObj('ModItemOptionChoice', {
	'name', "id_BleedingAmount_9mm",
	'DisplayName', "Bleeding damage 9mm",
	'Help', "Bleeding damage inflicted by 9mm caliber",
	'DefaultValue', "5 (Default)",
	'ChoiceList', {
		"0 (Disable bleeding)",
		"1",
		"2",
		"3",
		"4",
		"5 (Default)",
		"6",
		"7",
		"8",
		"9",
		"10",
		"11",
		"12",
		"13",
		"14",
		"15",
		"16",
		"17",
		"18",
		"19",
		"20 (Are you serious?)",
	},
}),
PlaceObj('ModItemOptionChoice', {
	'name', "id_BleedingAmount_Other",
	'DisplayName', "Bleeding inflicted by other calibers",
	'Help', "Bleeding damage inflicted by calibers added by mods or future patches",
	'DefaultValue', "5 (Default)",
	'ChoiceList', {
		"0 (Disable bleeding)",
		"1",
		"2",
		"3",
		"4",
		"5 (Default)",
		"6",
		"7",
		"8",
		"9",
		"10",
		"11",
		"12",
		"13",
		"14",
		"15",
		"16",
		"17",
		"18",
		"19",
		"20 (Are you serious?)",
	},
}),
PlaceObj('ModItemOptionToggle', {
	'name', "id_EnableFriendlyFireBleeding",
	'DisplayName', "Friendly fire inflicts bleeding",
	'DefaultValue', true,
}),
}
