UndefineClass('BleedingStacks')
DefineClass.BleedingStacks = {
	__parents = { "StatusEffect" },
	__generated_by_class = "ModItemCharacterEffectCompositeDef",


	object_class = "StatusEffect",
	msg_reactions = {
		PlaceObj('MsgReaction', {
			Event = "StatusEffectRemoved",
			Handler = function (self, obj, id, stacks, reason)
				local reaction_idx = table.find(self.msg_reactions or empty_table, "Event", "StatusEffectRemoved")
				if not reaction_idx then return end
				
				local function exec(self, obj, id, stacks, reason)
				local effect = obj:GetStatusEffect("BleedingStacks")
				if g_Teams[g_CurrentTeam] == obj.team and not obj:HasStatusEffect("BeingBandaged") then
					obj:GainAP(-self:ResolveValue("APLoss") * (effect.stacks + 1) / self:ResolveValue("APLossFactor") * const.Scale.AP)
				end
				end
				local _id = GetCharacterEffectId(self)
				if _id == id then exec(self, obj, id, stacks, reason) end
				
			end,
			HandlerCode = function (self, obj, id, stacks, reason)
				local effect = obj:GetStatusEffect("BleedingStacks")
				if g_Teams[g_CurrentTeam] == obj.team and not obj:HasStatusEffect("BeingBandaged") then
					obj:GainAP(-self:ResolveValue("APLoss") * (effect.stacks + 1) / self:ResolveValue("APLossFactor") * const.Scale.AP)
				end
			end,
			param_bindings = false,
		}),
		PlaceObj('MsgReaction', {
			Event = "UnitEndTurn",
			Handler = function (self, unit)
				local reaction_idx = table.find(self.msg_reactions or empty_table, "Event", "UnitEndTurn")
				if not reaction_idx then return end
				
				local function exec(self, unit)
				local effect = unit:GetStatusEffect("BleedingStacks")
				if not IsInCombat() then return end
				if unit:HasStatusEffect("BeingBandaged") then
					for i = effect.stacks, 1, -1 do
						unit:RemoveStatusEffect("BleedingStacks")
					end
					return
				end
				local dmgresult = self:ResolveValue("DamagePerTurn") * effect.stacks
				local floating_text = T{193053798048, "<num> (bleeding)", num = dmgresult}
				local pov_team = GetPoVTeam()
				local has_visibility = HasVisibilityTo(pov_team, unit)
				local log_msg = T{729241506274, "<name> bleeds for <em><num> damage</em>", name = unit:GetLogName(), num = dmgresult}
				unit:TakeDirectDamage(dmgresult, has_visibility and floating_text or false, "short", log_msg)
				end
				local id = GetCharacterEffectId(self)
				
				if id then
					if IsKindOf(unit, "StatusEffectObject") and unit:HasStatusEffect(id) then
						exec(self, unit)
					end
				else
					exec(self, unit)
				end
				
			end,
			HandlerCode = function (self, unit)
				local effect = unit:GetStatusEffect("BleedingStacks")
				if not IsInCombat() then return end
				if unit:HasStatusEffect("BeingBandaged") then
					for i = effect.stacks, 1, -1 do
						unit:RemoveStatusEffect("BleedingStacks")
					end
					return
				end
				local dmgresult = self:ResolveValue("DamagePerTurn") * effect.stacks
				local floating_text = T{193053798048, "<num> (bleeding)", num = dmgresult}
				local pov_team = GetPoVTeam()
				local has_visibility = HasVisibilityTo(pov_team, unit)
				local log_msg = T{729241506274, "<name> bleeds for <em><num> damage</em>", name = unit:GetLogName(), num = dmgresult}
				unit:TakeDirectDamage(dmgresult, has_visibility and floating_text or false, "short", log_msg)
			end,
			param_bindings = false,
		}),
		PlaceObj('MsgReaction', {
			Event = "UnitBeginTurn",
			Handler = function (self, unit)
				local reaction_idx = table.find(self.msg_reactions or empty_table, "Event", "UnitBeginTurn")
				if not reaction_idx then return end
				
				local function exec(self, unit)
				local effect = unit:GetStatusEffect("BleedingStacks")
				unit:ConsumeAP(1 * effect.stacks / 4 * const.Scale.AP)
				end
				local id = GetCharacterEffectId(self)
				
				if id then
					if IsKindOf(unit, "StatusEffectObject") and unit:HasStatusEffect(id) then
						exec(self, unit)
					end
				else
					exec(self, unit)
				end
				
			end,
			HandlerCode = function (self, unit)
				local effect = unit:GetStatusEffect("BleedingStacks")
				unit:ConsumeAP(1 * effect.stacks / 4 * const.Scale.AP)
			end,
			param_bindings = false,
		}),
		PlaceObj('MsgReaction', {
			Event = "GatherCTHModifications",
			Handler = function (self, attacker, cth_id, data)
				local reaction_idx = table.find(self.msg_reactions or empty_table, "Event", "GatherCTHModifications")
				if not reaction_idx then return end
				
				local function exec(self, attacker, cth_id, data)
				if cth_id == self.id then
					data.mod_add = data.mod_add + self:ResolveValue("cth_penalty")
				end
				end
				local id = GetCharacterEffectId(self)
				
				if id then
					if IsKindOf(attacker, "StatusEffectObject") and attacker:HasStatusEffect(id) then
						exec(self, attacker, cth_id, data)
					end
				else
					exec(self, attacker, cth_id, data)
				end
				
			end,
			HandlerCode = function (self, attacker, cth_id, data)
				if cth_id == self.id then
					data.mod_add = data.mod_add + self:ResolveValue("cth_penalty")
				end
			end,
			param_bindings = false,
		}),
	},
	DisplayName = T(486437174920, --[[ModItemCharacterEffectCompositeDef BleedingStacks DisplayName]] "Bleeding"),
	Description = T(446816475256, --[[ModItemCharacterEffectCompositeDef BleedingStacks Description]] "This character will take <color EmStyle>Damage</color> each turn until they are <color EmStyle>bandaged</color>. <color EmStyle>AP decreased by <APLoss></color> for every <color EmStyle><APLossFactor> bleeding damage</color>."),
	type = "Debuff",
	Icon = "Mod/pRbkRtK/images/icons/bleeding_penetrated.png",
	max_stacks = 99,
	RemoveOnEndCombat = true,
	RemoveOnSatViewTravel = true,
	Shown = true,
}

