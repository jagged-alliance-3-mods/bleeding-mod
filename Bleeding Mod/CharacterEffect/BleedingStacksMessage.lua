UndefineClass('BleedingStacksMessage')
DefineClass.BleedingStacksMessage = {
	__parents = { "CharacterEffect" },
	__generated_by_class = "ModItemCharacterEffectCompositeDef",


	comment = "Replase with custom floating text function",
	object_class = "CharacterEffect",
	DisplayName = T(750385501935, --[[ModItemCharacterEffectCompositeDef BleedingStacksMessage DisplayName]] "Bleeding"),
	RemoveEffectText = T(923209475182, --[[ModItemCharacterEffectCompositeDef BleedingStacksMessage RemoveEffectText]] "<color EmStyle><DisplayName></color>"),
	lifetime = "Until End of Turn",
	RemoveOnEndCombat = true,
	RemoveOnSatViewTravel = true,
	HasFloatingText = true,
}

